﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidHW;

internal class GuessingGameSeed : GuessingGame
{
    public int? Seed {  get; private set; }
    
    public GuessingGameSeed(int minNum = 1, int maxNum = 100, int tryCount = 7, int? seed = null)
        : base(minNum, maxNum, tryCount)
    {
        Seed = seed;
    }

    public override void StartGame()
    {
        if (Seed.HasValue) 
        {
            _desiredNum = new Random(Seed.Value).Next(_minNum, _maxNum + 1);
        }
        else
        {
            _desiredNum = new Random().Next(_minNum, _maxNum + 1);
        }

        Console.WriteLine($"\nЯ загадал число от {_minNum} до {_maxNum}.");
        Console.WriteLine($"У тебя {_tryCount} попыток.\n");

        Play();
    }
}
